El objetivo de este proyecto es configurar almacenamiento remeto
por medio del protocolo iSCSI que hace uso de TCP/IP para 
transmitir mensajes SCSI

iscsi consta de un servidor o "target" en el que se configura el 
almacenamiento que se desea exponer a los clientes y un cliente
o "initiator" que utiliza el almecanamiento remote como si fuese 
almacenamiento local.



Target:
Un ubuntu server 20.04 que consta de dos discos físicos
EN un disco esta instalado el sistema operativo(dev/vda) y otro disco
se configura como target de iscsi (/dev/vdb)
Esta máquina tiene asignada la ip 192.168.122.113
Initiator:
También un ubuntu server 20.04 en el cual montaremos el target remoto
como disco local a través de open-iscsi (cliente de iscsi). Tiene 
ip 192.168.122.162

En esta práctica utilizaremos dos máquinas virtuales creadas con
virt-manager pero pueden usarse cualquiera de programas de virtualización
disponibles como virtual-box, vmware, HyperV, etc.
Lo importante es que el servidor iscsi tenga dos discos para usar uno como target

En este caso ambas máquinas se encuentran en la misma red local pero iscsi funciona
también en máquinas conectadas a internet


+-----------------+	+++++++++++++++++++	 +-----------------+
| iSCSI Target    |	|		  |      | iSCSI Initiator |
| 192.168.122.113 +-----| Red Local       |------+ 192.168.122.162 |
|                 |     | 192.168.122.0/24|	 |                 |
+-----------------+	+++++++++++++++++++      +-----------------+




#################################################################################################
Configuración de iSCSI Target

Salida de fdisk -l

Disk /dev/vda: 20 GiB, 21474836480 bytes, 41943040 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: E0C7BB86-4032-437F-9E16-F114B81F35D4

Device       Start      End  Sectors Size Type
/dev/vda1     2048     4095     2048   1M BIOS boot
/dev/vda2     4096  2101247  2097152   1G Linux filesystem
/dev/vda3  2101248 41940991 39839744  19G Linux filesystem


Disk /dev/vdb: 20 GiB, 21474836480 bytes, 41943040 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes

Usaremos /dev/vdb como target 

En esta práctica ya tenemos la ip estática asignada

Primero actualizamos paquetería
~$sudo apt update && sudo apt upgrade -y

Instalamos el servidor iscsi
~$sudo apt-get install tgt -y

Verificamos que el servicio esté corriendo
~$systemctl status tgt.service 
● tgt.service - (i)SCSI target daemon
     Loaded: loaded (/lib/systemd/system/tgt.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2020-10-20 03:09:48 UTC; 15s ago
       Docs: man:tgtd(8)
   Main PID: 12412 (tgtd)
     Status: "Starting event loop..."
      Tasks: 1
     Memory: 1.1M
     CGroup: /system.slice/tgt.service
             └─12412 /usr/sbin/tgtd -f
ubuntu-nas systemd[1]: Starting (i)SCSI target daemon...
ubuntu-nas tgtd[12992]: tgtd: iser_ib_init(3431) Failed to initialize RDMA; load kernel modules?
ubuntu-nas tgtd[12992]: tgtd: work_timer_start(146) use timer_fd based scheduler
ubuntu-nas tgtd[12992]: tgtd: bs_init(387) use signalfd notification
ubuntu-nas tgtd[12992]: tgtd: device_mgmt(246) sz:14 params:path=/dev/vdb
ubuntu-nas tgtd[12992]: tgtd: bs_thread_open(409) 16
ubuntu-nas systemd[1]: Started (i)SCSI target daemon.


Una vez instalado el servidor procedemos a crear un dispositivo LUN o "Logical Unit Number"

Una LUN es una abstracción de almacenamiento, donde una partición, disco o un arreglo de discos, se 
presenta  como un disco local físicamente conectado a través de alguna conexión ya sea iSCSI 
(internet Small Computer System Interface) o FC (Fiber Channel) .

Para configurar nuestro target creamos el archivo /etc/tgt/conf.d/iscsi.conf y asignamos 
el dispositivo a convertir en LUN, la dirección del cliente que permitiremos conectar y 
los usuarios necesarios para implementar la seguridad de 2 etapas llamada MCHAP en la conexión 

~$sudo vim /etc/tgt/conf.d/iscsi.conf
<target iqn.2020-10.ubuntu-nas:lun1>
     backing-store /dev/vdb
     initiator-address 192.168.122.162
     incominguser iscsi-user PasswordFiable
     outgoinguser iscsi-target OtroPasswordFiable
</target>

Se reinicia el servicio
~$sudo systemctl restart tgt

Luego utilizamos el comando tgtadm para verificar la creación del target
~$sudo tgtadm --mode target --op show
Target 1: iqn.2020-10.ubuntu-nas:lun1
    System information:
        Driver: iscsi
        State: ready
    I_T nexus information:
    LUN information:
        LUN: 1
            Type: disk
            SCSI ID: IET     00010001
            SCSI SN: beaf11
            Size: 21475 MB, Block size: 512
            Online: Yes
            Removable media: No
            Prevent removal: No
            Readonly: No
            SWP: No
            Thin-provisioning: No
            Backing store type: rdwr
            Backing store path: /dev/vdb
            Backing store flags: 
    Account information:
        iscsi-user
        iscsi-target (outgoing)
    ACL information:
        192.168.122.113


Podemos confirmar que el servicio esta esperando conexiones el el puerto 3260 con ss

~$ss -ltn
State   Recv-Q Send-Q Local Address:Port   Peer Address:Port Process                
LISTEN  0      4096   127.0.0.53%lo:53          0.0.0.0:*                           
LISTEN  0      128          0.0.0.0:22          0.0.0.0:*                           
LISTEN  0      128          0.0.0.0:3260        0.0.0.0:*                           
LISTEN  0      128             [::]:22             [::]:*                           
LISTEN  0      128             [::]:3260           [::]:*                          

Ya tenemos configurado el target y ya podemos ir a configurar el cliente que
usará este target como almacenamiento 


#####################################################################################
Configuración de iSCSI Initiator
Instalamos el cliente de iscsi
~$apt-get install open-iscsi -y

Utilizamos el comando iscsiadm para "descubrir" el target remoto
~$sudo iscsiadm -m discovery -t st -p 192.168.122.113
192.168.122.113:3260,1 iqn.2020-10.ubuntu-nas:lun1

Ahora debemos escribir el nombre del LUN remoto en el archivo  /etc/iscsi/initiatorname.iscsi

~$sudo vim  /etc/iscsi/initiatorname.iscsi
InitiatorName=iqn.2020-10.ubuntu-nas:lun1

A continuación, deberá definir la información de autenticación MCHAP para acceder al target  desde el initiator.
El archivo de configuración del nodo existirá en el directorio "/etc/iscsi/nodes/" que tendrá además
un directorio por LUN disponible.

~$sudo vim /etc/iscsi/nodes/iqn.2020-10.ubuntu-nas:lun1/192.168.122.113,3260,1/default
#Agregar y/o modificar las siguientes líneas según sea necesario
node.startup = automatic
node.session.auth.authmethod = CHAP
node.session.auth.username = iscsi-user
node.session.auth.password = PasswordFiable
node.session.auth.username_in = iscsi-target
node.session.auth.password_in = OtroPasswordFiable

Una vez guardados los cambios pasamos reiniciamos el cliente
~$sudo systemctl restart open-iscsi iscsid

Verificamos el estado del servicio
~$ systemctl status open-iscsi
● open-iscsi.service - Login to default iSCSI targets
     Loaded: loaded (/lib/systemd/system/open-iscsi.service; enabled; vendor preset: enabled)
     Active: active (exited) since Tue 2020-10-20 04:32:08 UTC; 1min 24s ago
       Docs: man:iscsiadm(8)
             man:iscsid(8)
    Process: 22620 ExecStart=/sbin/iscsiadm -m node --loginall=automatic (code=exited, status=0/SUCCESS)
    Process: 22654 ExecStart=/lib/open-iscsi/activate-storage.sh (code=exited, status=0/SUCCESS)
   Main PID: 22654 (code=exited, status=0/SUCCESS)

iscsi-client systemd[1]: Starting Login to default iSCSI targets...
iscsi-client iscsiadm[22620]: Logging in to [iface: default, target: iqn.2020-10.ubuntu-nas:lun1, portal: 192.168.122.113,3260] (multiple)
iscsi-client iscsiadm[22620]: Login to [iface: default, target: iqn.2020-10.ubuntu-nas:lun1, portal: 192.168.122.113,3260] successful.
iscsi-client systemd[1]: Finished Login to default iSCSI targets.

También podemos verificarlo con el comando iscsiadm
~$ iscsiadm -m session -o show
tcp: [1] 192.168.122.113:3260,1 iqn.2020-10.ubuntu-nas:lun1 (non-flash)

Ejecutamos fdisk y podemos ver que tenemos un nuevo disco /dev/sda disponible

~$sudo fdisk -l
Disk /dev/vda: 20 GiB, 21474836480 bytes, 41943040 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 170797D9-0938-4995-AC92-7B81600DF15F

Device       Start      End  Sectors Size Type
/dev/vda1     2048     4095     2048   1M BIOS boot
/dev/vda2     4096  2101247  2097152   1G Linux filesystem
/dev/vda3  2101248 41940991 39839744  19G Linux filesystem




Disk /dev/mapper/ubuntu--vg-ubuntu--lv: 18.102 GiB, 20396900352 bytes, 39837696 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/sda: 20 GiB, 21474836480 bytes, 41943040 sectors
Disk model: VIRTUAL-DISK    
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes

Este nuevo disco estará disponible incluso si se reinicia el sistema y pueden
usarse herramientas como fdisk y mkfs.ext4 para particionar y dar formato como si 
estuviésemos trabajando en un disco físico local en el sistema.

