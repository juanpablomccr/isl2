#!/bin/bash
#Script didactico de bucle for
#script creado para agregar cuentas de usuario:

#Archivo destino de cuanteas con su password
touch /root/password
echo -e "User\tPassword" > root/password
echo -e "----\t--------" >> root/password

#bucle for
for name in "$(seq -f "usuario%g" 0 20)"
do
	useradd "$name"
	pw="$(tr -dc A-Za-z0-9_ < /dev/urandom | head -c 12 | xargs)"
	echo -e "$name:\t$pw" >> /root/password
	echo "$name":"$pw" | chpasswd
	chage -M 90 -m 0 -W 10 "$name"
done

