#!/bin/bash
#Script de ejemplo de sustitución de comandos en un script

#Generación de reporte de uso de Filesystem 
echo "Reporte de uso de Fylesystem" | tee -a /tmp/report
ls . > dir.txt
#redicción de salida de comando a variable
start=$(date)
#redirección de salida de comando a argumento 
du -sh $(cat dir.txt) | tee -a /tmp/report
echo "Inicio de reporte: $start" | tee -a /tmp/report
echo "Fin del reporte: $(date)" | tee -a /tmp/report
