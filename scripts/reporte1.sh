#!/bin/bash
#Solicitar directorio al usuario final
read -p "Ingresar directorio a generar reporte de uso: " dir
start=$(date)
echo "Reporte de uso para $dir:" >> /tmp/reporte
du -sh $dir >> /tmp/reporte 2> /dev/null
if [ $? -eq  0 ]
then
	echo "Inicio del reporte: $start" >> /tmp/reporte
	echo "Final del  reporte: $(date)" >> /tmp/reporte
else
        echo "Error: no se generó reporte.  $dir inaccessible o inexistente" | tee -a /tmp/reporte
fi
