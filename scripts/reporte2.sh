#!/bin/bash
#Solicitar entrada al usuario 
read -p "Ingreasr el directorio a generar reporte de uso: " dir
start=$(date)
echo "Reporte de uso de $dir" >> /tmp/reporte2
if du -sh $dir >> /tmp/reporte2 2> /dev/null
then
	echo "Inicio de reporte: $start" >> /tmp/reporte2
	echo "Fin de reporte: $(date)" >> /tmp/reporte2
else
	echo "Error: Reporte no generado.  $dir inaccessible o inexistente" | tee -a /tmp/reporte2
fi
