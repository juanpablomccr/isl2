#!/bin/bash
#Script sin verificación de entrada
#Solicitar al usuario un zip code 
#(entrada debe ser solo numeros con o sin guiones)
read -p "Ingrese su código ZIP : " zip
#Aplicar test para detectar formato inválido de código
while echo $zip | grep -E -v '^[0-9]{5}$' > /dev/null 2>/dev/null
do
	echo "formato incorrecto de código"                                  
	read -p "Ingrese su código ZIP válido : " zip
done
echo "Gracias por ingresar su código"         
