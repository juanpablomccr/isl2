#!/bin/bash
start=$(date)
#Solicitar entrada al usuario
read -p "Ingrese archivo a obtener reporte de uso: " dir
echo "Reporte de uso de $dir" >> /tmp/reporte3
#Verificar si archivo ingresado es directorio y tiene permisos de lectura y ejecución 
if [ -d $dir -a -r $dir -a -x $dir ]
then
	du -sh $dir >> /tmp/reporte3 2> /dev/null
	echo "Inicio de reporte: $start" >> /tmp/reporte3
	echo "Fin de reporte: $(date)" >> /tmp/reporte3
else
	echo "Error: reporte no generado.  $dir inaccessible o inexistente" | tee -a  /tmp/reporte3
fi
