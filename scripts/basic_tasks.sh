#!/bin/bash
#################################################################################
#Script adaptado de script creado por Sathish Arthar (Sathisharthar {at} gmail.com) 
#publicado en Jan 2014 en el sitio web:
# https://www.techbrown.com/most-useful-bash-scripts-linux-sysadmin/
#################################################################################
#script básico de bash para obtner infomración de un  sistema corriendo linux

# Definición de  variables
LSB=/usr/bin/lsb_release

# Purpose: Display pause prompt
# $1-> Message (optional)
function pause(){
local message="$@"
[ -z $message ] && message="Press [Enter] key to continue..."
read -p "$message" readEnterKey
}

# Purpose - Display a menu on screen
function show_menu(){
date
echo "---------------------------"
echo " Main Menu"
echo "---------------------------"
echo "1. Operating system info"
echo "2. Hostname and dns info"
echo "3. Network info"
echo "4. Who is online"
echo "5. Last logged in users"
echo "6. Free and used memory info"
echo "7. Get my ip address"
echo "8. My Disk Usage"
echo "9. Exit"
}

# Purpose - Display header message
# $1 - message
function write_header(){
local h="$@"
echo "---------------------------------------------------------------"
echo " ${h}"
echo "---------------------------------------------------------------"
}

# Purpose - Get info about your operating system
function os_info(){
write_header " System information "
echo "Operating system : $(uname)"
[ -x $LSB ] && $LSB -a || echo "$LSB command is not insalled (set \$LSB variable)"
#pause "Press [Enter] key to continue..."
pause
}

# Purpose - Get info about host such as dns, IP, and hostname
function host_info(){
write_header " Hostname and DNS information "
echo "Hostname : $(hostnamectl status | grep hostname)"
echo "DNS name servers (DNS IP) :"
resolvectl dns
pause
}

# Purpose - Network inferface and routing info
function net_info(){

echo "*** IP Addresses Information ***"
ip a | grep inet
echo "***********************"
echo "*** Network routing ***"
echo "***********************"
ip route
pause 
}

# Purpose - Display a list of users currently logged on 
# display a list of receltly loggged in users 
function user_info(){
local cmd="$1"
case "$cmd" in 
who) write_header " Who is online "; who -H; pause ;;
last) write_header " List of last logged in users "; last ; pause ;;
esac 
}

# Purpose - Display used and free memory info
function mem_info(){
write_header " Free and used memory "
free -h
echo "***********************************"
echo "*** Top 5 memory eating process ***"
echo "***********************************" 
ps auxf | sort -nr -k 4 | head -5 
pause
}

# Purpose - Get Public IP address form your ISP
function ip_info(){
cmd='curl -s'
write_header " Public IP information "
ipservice=checkip.dyndns.org
pipecmd=(sed -e 's/.*Current IP Address: //' -e 's/<.*$//') #using brackets to use it as an array and avoid need of scaping
$cmd "$ipservice" | "${pipecmd[@]}"
pause
}

# purpose - Get Disk Usage Information
function disk_info() {
EXCLUDE_LIST=""
write_header " Disk Usage Info"
if [ "$EXCLUDE_LIST" != "" ] ; then
  df -H | grep -vE "^Filesystem|tmpfs|cdrom|${EXCLUDE_LIST}" | awk '{print $5 " " $6}'
else
  df -H | grep -vE "^Filesystem|tmpfs|cdrom" | awk '{print $5 " " $6}'
fi
pause
}

# Purpose - Get input via the keyboard and make a decision using case..esac 
function read_input(){
local c
read -p "Enter your choice [ 1 - 9 ] " c
case $c in
1) os_info ;;
2) host_info ;;
3) net_info ;;
4) user_info "who" ;;
5) user_info "last" ;;
6) mem_info ;;
7) ip_info ;;
8) disk_info ;;
9) echo "Bye!"; exit 0 ;;
*) 
echo "Please select between 1 to 9 choice only."
pause
esac
}

# ignore CTRL+C, CTRL+Z and quit singles using the trap
trap '' SIGINT SIGQUIT SIGTSTP

# main logic
while true
do
clear
show_menu # display memu
read_input # wait for user input
done
