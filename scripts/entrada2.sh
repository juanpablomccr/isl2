#!/bin/bash
#Script sin verificación de entrada
#Solicitar al usuario un zip code 
#(entrada debe ser solo numeros con o sin guiones)
read -p "Ingrese su código ZIP : " zip
#Aplicar test para detectar formato inválido de código
if echo $zip | grep -E '^[0-9]+$' > /dev/null 2>/dev/null
then                                                    
     echo "Gracias por ingresar su código"            
else                                                    
     echo "formato incorrecto de código"                           
fi         
