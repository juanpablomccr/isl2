#!/bin/bash
# ejemplo_read.sh Script para ejemplificar el uso de la directiva read
#La opción -p se utiliza para mostrar un mensaje en la terminal

read -p "Ingrese ruta a carpeta: " dir
start=$(date)
echo "Reporte de uso de filesystem" | tee /tmp/report
du -sh $dir | tee -a /tmp/report
echo "Start of report: $start" | tee -a  /tmp/report
echo "End of report: $(date)" | tee -a /tmp/report
